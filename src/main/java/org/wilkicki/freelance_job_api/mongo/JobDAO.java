package org.wilkicki.freelance_job_api.mongo;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.bson.types.ObjectId;
import org.mongojack.JacksonMongoCollection;
import org.wilkicki.freelance_job_api.core.Job;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;

public class JobDAO {

  private MongoDatabase database;
  private JacksonMongoCollection<Job> jobCollection;

  public JobDAO(MongoDatabase database) {
    this.database = database;
    this.jobCollection = JacksonMongoCollection.builder().build(this.database, Job.class);
  }

  public void add(Job... jobs) {
    this.jobCollection.insert(jobs);
  }

  public void update(Job job) {
    UpdateResult result =
        this.jobCollection.replaceOne(Filters.eq("_id", new ObjectId(job.getId())), job);
    if (result.getModifiedCount() != 1) {
      throw new IllegalStateException(String.format("Failed to update job '%s'", job));
    }
  }

  public Job findById(ObjectId id) {
    return this.jobCollection.findOne(Filters.eq("_id", id));
  }

  public List<Job> findAllJobs() {
    return StreamSupport.stream(jobCollection.find().spliterator(), false)
        .collect(Collectors.toList());
  }

}
