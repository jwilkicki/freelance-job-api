package org.wilkicki.freelance_job_api.mongo;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.mongojack.JacksonMongoCollection;
import org.wilkicki.freelance_job_api.core.Artist;
import com.mongodb.client.MongoDatabase;

public class ArtistDAO {

  private MongoDatabase mongoDatabase;
  private JacksonMongoCollection<Artist> artistCollection;

  public ArtistDAO(MongoDatabase database) {
    this.mongoDatabase = database;
    this.artistCollection = JacksonMongoCollection.builder().build(mongoDatabase, Artist.class);
  }

  public void add(Artist... artists) {
    this.artistCollection.insert(artists);
  }

  public List<Artist> findAllArtists() {
    return StreamSupport.stream(artistCollection.find().spliterator(), false)
        .collect(Collectors.toList());
  }

}
