package org.wilkicki.freelance_job_api;

import org.wilkicki.freelance_job_api.bundles.mongo.MongoConfiguration;
import io.dropwizard.Configuration;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FreelanceJobAPIConfiguration extends Configuration {

  @NonNull
  protected MongoConfiguration mongo;
}
