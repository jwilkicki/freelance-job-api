package org.wilkicki.freelance_job_api;

import org.wilkicki.freelance_job_api.bundles.mongo.MongoBundle;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class FreelanceJobAPIApplication extends Application<FreelanceJobAPIConfiguration> {

  MongoBundle mongoBundle;

  public static void main(final String[] args) throws Exception {
    new FreelanceJobAPIApplication().run(args);
  }

  @Override
  public String getName() {
    return "FreelanceJobAPI";
  }

  @Override
  public void initialize(final Bootstrap<FreelanceJobAPIConfiguration> bootstrap) {
    bootstrap.addBundle(mongoBundle = new MongoBundle());
  }

  @Override
  public void run(final FreelanceJobAPIConfiguration configuration, final Environment environment) {
    MongoClient client = mongoBundle.getMongoClient();
    MongoDatabase database = client.getDatabase(configuration.getMongo().getDatabase());
  }

}
