package org.wilkicki.freelance_job_api.bundles.mongo;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MongoConfiguration {

  @NonNull
  protected String database;
  protected List<Server> seeds = new ArrayList<>();
  protected Credentials credentials;

  @Getter
  @Setter
  @NoArgsConstructor
  public static class Server {
    @NonNull
    protected String host;
    @NonNull
    protected Integer port;
  }

  @Getter
  @Setter
  @NoArgsConstructor
  public static class Credentials {
    @NonNull
    protected String userName;
    @NonNull
    protected String password;
  }
}
