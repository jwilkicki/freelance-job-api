package org.wilkicki.freelance_job_api.bundles.mongo;

import static java.util.stream.Collectors.toList;
import java.util.List;
import org.wilkicki.freelance_job_api.FreelanceJobAPIConfiguration;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import io.dropwizard.ConfiguredBundle;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@NoArgsConstructor
public class MongoBundle implements ConfiguredBundle<FreelanceJobAPIConfiguration> {

  @Getter
  @Setter
  @NonNull
  MongoConfiguration mongo;

  @Getter(lazy = true)
  private final MongoClient mongoClient = createMongoClient();

  private MongoClient createMongoClient() {
    return MongoClients.create(MongoClientSettings.builder()
        .applyToClusterSettings(builder -> builder.hosts(getServers())).build());
  }

  private List<ServerAddress> getServers() {
    return mongo.getSeeds().stream().map(this::toServerAddress).collect(toList());
  }

  private ServerAddress toServerAddress(MongoConfiguration.Server server) {
    return new ServerAddress(server.getHost(), server.getPort());
  }

  @Override
  public void run(FreelanceJobAPIConfiguration configuration, Environment environment)
      throws Exception {
    setMongo(configuration.getMongo());
    getMongoClient();
    environment.lifecycle().manage(new Managed() {
      @Override
      public void start() throws Exception {}

      @Override
      public void stop() throws Exception {
        getMongoClient().close();
      }
    });

  }
}
