package org.wilkicki.freelance_job_api.core;

import java.util.List;
import javax.persistence.Id;
import org.mongojack.MongoCollection;
import org.mongojack.ObjectId;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@MongoCollection(name = "artists")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Artist {

  @ObjectId
  @Id
  protected String id;
  @JsonProperty
  protected String firstName;
  @JsonProperty
  protected String lastName;

  protected List<String> skills;
}
