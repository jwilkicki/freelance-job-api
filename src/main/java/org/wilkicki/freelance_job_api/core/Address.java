package org.wilkicki.freelance_job_api.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Address {

  @JsonProperty
  protected String addressOne;

  @JsonProperty
  protected String addressTwo;

  @JsonProperty
  protected String city;

  @JsonProperty
  protected String state;

  @JsonProperty
  protected String postalCode;
}
