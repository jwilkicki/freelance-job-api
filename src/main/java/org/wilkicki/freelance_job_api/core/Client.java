package org.wilkicki.freelance_job_api.core;

import java.util.List;
import javax.persistence.Id;
import org.mongojack.ObjectId;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Client {

  @ObjectId
  @Id
  protected String id;
  @JsonProperty
  protected String firstName;
  @JsonProperty
  protected String lastName;
  @JsonProperty
  protected String email;
  @JsonProperty
  protected String cellNumber;
  @JsonProperty
  protected Address address;
  @JsonProperty
  protected List<Service> services;
}
