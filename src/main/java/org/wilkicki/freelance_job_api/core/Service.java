package org.wilkicki.freelance_job_api.core;

import org.bson.types.Decimal128;
import org.mongojack.DBRef;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Service {

  @JsonProperty
  protected String service;

  @JsonProperty
  protected Decimal128 price;

  @JsonProperty
  protected DBRef<Artist, String> assignedArtist;
}
