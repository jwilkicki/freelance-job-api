package org.wilkicki.freelance_job_api.core;

import java.util.Date;
import java.util.List;
import javax.persistence.Id;
import org.mongojack.DBRef;
import org.mongojack.MongoCollection;
import org.mongojack.ObjectId;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@MongoCollection(name = "jobs")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Job {

  @ObjectId
  @Id
  protected String id;

  @JsonProperty
  protected Date eventDate;

  @JsonProperty
  protected Address address;

  @JsonProperty
  protected String description;

  @JsonProperty
  protected DBRef<Artist, String> leadArtist;

  @JsonProperty
  protected List<DBRef<Artist, String>> artists;

  @JsonProperty
  protected List<Client> clients;

}
