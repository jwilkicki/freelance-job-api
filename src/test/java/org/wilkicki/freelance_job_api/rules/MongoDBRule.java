package org.wilkicki.freelance_job_api.rules;

import java.util.Arrays;
import org.junit.rules.ExternalResource;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;

public class MongoDBRule extends ExternalResource {

  @Getter
  private MongoClient client;
  @Getter
  private MongoDatabase database;

  public MongoDBRule(String databaseName) {
    this.client = MongoClients.create(MongoClientSettings.builder()
        .applyToClusterSettings(builder -> builder.hosts(Arrays.asList(
            new ServerAddress("localhost", Integer.parseInt(System.getProperty("mongoPort"))))))
        .build());
    this.database = client.getDatabase(databaseName);
  }

  @Override
  protected void before() throws Throwable {

  }

  @Override
  protected void after() {
    client.close();
  }



}
