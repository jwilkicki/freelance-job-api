package org.wilkicki.freelance_job_api.mongo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.wilkicki.freelance_job_api.core.Job;
import org.wilkicki.freelance_job_api.rules.MongoDBRule;

public class JobDAOIT {

  @ClassRule
  public static MongoDBRule mongoDBRule = new MongoDBRule(JobDAOIT.class.getSimpleName());

  JobDAO jobDAO;

  @Before
  public void setup() {
    jobDAO = new JobDAO(mongoDBRule.getDatabase());
  }

  @Test
  public void testInsert() {
    Job expectedJob = new Job();
    expectedJob.setDescription("This is my description");

    jobDAO.add(expectedJob);
    assertThat(expectedJob, hasProperty("id", notNullValue()));

    List<Job> jobs = jobDAO.findAllJobs();

    assertThat(jobs, not(empty()));
    assertThat(jobs, hasItem(hasProperty("description", equalTo(expectedJob.getDescription()))));
  }

  @Test
  public void testUpdate() {
    Job expectedJob = new Job();
    expectedJob.setDescription("This is a new description");

    jobDAO.add(expectedJob);

    expectedJob.setEventDate(Date.from(Instant.now()));

    jobDAO.update(expectedJob);

    Job actualJob = jobDAO.findById(new ObjectId(expectedJob.getId()));

    assertThat(actualJob, hasProperty("eventDate", equalTo(expectedJob.getEventDate())));
  }

}
