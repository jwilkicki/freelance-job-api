package org.wilkicki.freelance_job_api.mongo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import java.util.List;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.wilkicki.freelance_job_api.core.Artist;
import org.wilkicki.freelance_job_api.rules.MongoDBRule;

public class ArtistDAOIT {

  @ClassRule
  public static MongoDBRule mongoDBRule = new MongoDBRule(ArtistDAOIT.class.getSimpleName());

  ArtistDAO artistDAO;

  @Before
  public void setup() {
    artistDAO = new ArtistDAO(mongoDBRule.getDatabase());
  }

  @Test
  public void testInsert() {
    Artist expectedArtist = new Artist();
    expectedArtist.setFirstName("Jane");
    expectedArtist.setLastName("Doe");

    artistDAO.add(expectedArtist);
    assertThat(expectedArtist, hasProperty("id", notNullValue()));

    List<Artist> artists = artistDAO.findAllArtists();

    assertThat(artists, not(empty()));
    assertThat(artists,
        hasItem(allOf(hasProperty("firstName", equalTo(expectedArtist.getFirstName())),
            hasProperty("lastName", equalTo(expectedArtist.getLastName())))));
  }

}
